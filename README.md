# G-Data AntiVirus Business PRTG Sensor

Monitors:

- Total number of clients
- Number of clients where the signature update is older than 3 days
- Number of clients where LastAccess, ProgrammVersion etc is empty -> Failed installation
- Number of not fixed security issues of the lasst 10 days

See: https://kb.paessler.com/en/topic/70618-how-to-setup-the-sql-v2-sensors-in-prtg-is-there-a-guide