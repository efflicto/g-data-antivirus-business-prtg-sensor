SELECT
  ClientsCount = (SELECT
      Count(GDATA_AntiVirus_ManagementServer.dbo.client.ID) AS ClientsCount
    FROM
      GDATA_AntiVirus_ManagementServer.dbo.client
    WHERE
      GDATA_AntiVirus_ManagementServer.dbo.client.MachineName NOT LIKE '%~::%'),
  ClientsSigUpdateOld = (SELECT
      Count(GDATA_AntiVirus_ManagementServer.dbo.client.ID) AS ClientsCount
    FROM
      GDATA_AntiVirus_ManagementServer.dbo.client
    WHERE
      GDATA_AntiVirus_ManagementServer.dbo.client.MachineName NOT LIKE '%~::%' AND
      DateAdd(day, -3, GetDate()) > GDATA_AntiVirus_ManagementServer.dbo.client.SigUpdateDate),
  ClientsFailed = (SELECT
      Count(GDATA_AntiVirus_ManagementServer.dbo.client.ID) AS ClientsCount
    FROM
      GDATA_AntiVirus_ManagementServer.dbo.client
    WHERE
      GDATA_AntiVirus_ManagementServer.dbo.client.MachineName NOT LIKE '%~::%' AND
      GDATA_AntiVirus_ManagementServer.dbo.client.LastAccess IS NULL),
  SecurityIssues = (SELECT
      Count(GDATA_AntiVirus_ManagementServer.dbo.report.ID) AS SecurityReports
    FROM
      GDATA_AntiVirus_ManagementServer.dbo.report
    WHERE
      GDATA_AntiVirus_ManagementServer.dbo.report.Date > DateAdd(day, -10, GetDate()) AND
      GDATA_AntiVirus_ManagementServer.dbo.report.FileName NOT LIKE 'G DATA Security Client' AND
      GDATA_AntiVirus_ManagementServer.dbo.report.IsFixed = 0)